package assignment_2;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.HFileOutputFormat2;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import java.io.IOException;

public class BulkImportMapReduce extends Configured implements Tool {

    private static final String DATA_SEPERATOR = ",";
    private static final String TABLE_NAME = "EmployeeDetailsBulkLoad";
    private static final String COLUMN_FAMILY_1 = "PersonalDetails";
    private static final String COLUMN_FAMILY_2 = "contactDetails";
    private final Configuration configuration;

    public BulkImportMapReduce() {
        configuration = getConf();
        configuration.set("hbase.table.name", TABLE_NAME);
        configuration.set("COLUMN_FAMILY_1", COLUMN_FAMILY_1);
    }

    /**
     * args[0]: HDFS input path
     * args[1]: HDFS output path
     */

    public static void main(String[] args) {
        try {
            int response = ToolRunner.run(HBaseConfiguration.create(), new BulkImportMapReduce(), args);
            if (response == 0) {
                System.out.println("Job is successfully completed...");
            } else {
                System.out.println("Job failed...");
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    /*
    Refactor comments: Segregated into 3 internal functions to increase readability
    Moved configuration setting to constructor
     */
    @Override
    public int run(String[] args) throws Exception {
        int result = 0;

        try {
            Job job = new Job(configuration);
            job.setJarByClass(BulkImportMapReduce.class);    // class that contains mapper

            // For initializing Mapper and Reducer
            initializeMapReduceJob(job);

            //For configuring connections and output format classes
            configureConnection(args[0], job);

            boolean jobStatus = job.waitForCompletion(true);
            if (job.isSuccessful()) {
                HBaseBulkLoad hBaseBulkLoadObject = new HBaseBulkLoad();
                hBaseBulkLoadObject.doBulkLoad(args[0], TABLE_NAME);
            } else {
                result = -1;
            }
            return jobStatus ? 0 : 1;
        } catch (IOException ex) {
            System.out.println("EXCEPTION: " + ex.getMessage());
            ex.printStackTrace();
        }
        return 0;
    }

    private void configureConnection(String arg, Job job) throws IOException {
        Connection conn = ConnectionFactory.createConnection(HBaseConfiguration.create());
        TableName tableName = TableName.valueOf(Bytes.toBytes("EmployeeDetails"));
        HFileOutputFormat2.configureIncrementalLoad(job, conn.getTable(tableName), conn.getRegionLocator(tableName));
        job.setOutputFormatClass(HFileOutputFormat2.class);
        HFileOutputFormat2.setOutputPath(job, new Path("/MapReduceJobs/HFilesGenerated"));

        Path targetPath = new Path(arg);
        FileOutputFormat.setOutputPath(job, targetPath);
    }

    private void initializeMapReduceJob(Job job) throws IOException {
        Scan scan = new Scan();
        scan.setCaching(500);        // 1 is the default in Scan, which will be bad for MapReduce jobs
        scan.setCacheBlocks(false);  // don't set to true for MR jobs
        scan.addFamily(Bytes.toBytes("PersonalDetails"));

        TableMapReduceUtil.initTableMapperJob(
                "EmployeeDetails",      // input table
                scan,             // Scan instance to control CF and attribute selection
                assignment_2.HBaseBulkLoadMapper.class,   // mapper class
                ImmutableBytesWritable.class,             // mapper output key
                Put.class,             // mapper output value
                job);
        job.setNumReduceTasks(0);
    }
}
