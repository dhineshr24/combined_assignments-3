package assignment_4.First;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.HFileOutputFormat2;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;

/*
@args:
args[0]: TableName
args[1]: Input filepath in hdfs
args[2]: Output filepath for the HFile
 */

//made constant variables final
//Handled specific exceptions instead of catching generic exceptions
//Moved initialization of connections to new method "initializeConnections"
public class BulkLoadMain extends Configured implements Tool {
    static Connection connection = null;
    static Table table = null;

    public static void initializeConnections(Configuration conf, String tableName) throws IOException{
        connection = ConnectionFactory.createConnection(conf);
        table = connection.getTable(TableName.valueOf(tableName));
    }

    @Override
    public int run(String[] args) throws IOException, InterruptedException, ClassNotFoundException {
        String tableName = args[0];
        Configuration conf = super.getConf();
        final String hdfsPath = "hdfs://" + "localhost:9000";
        conf.set("fs.default.name", hdfsPath);
        //Connection connection = ConnectionFactory.createConnection(conf);
        //Table table = connection.getTable(TableName.valueOf(tableName));
        try{
            initializeConnections(conf, tableName);
        }
        catch(IOException ex){
            ex.printStackTrace();
        }
        Job job = Job.getInstance(conf, tableName + "BulkLoad");
        job.setInputFormatClass(TextInputFormat.class);
        TextInputFormat.addInputPath(job,new Path(args[1]));
        //Define our mapper class
        job.setMapperClass(BulkLoadMapper.class);
        job.setMapOutputKeyClass(ImmutableBytesWritable.class);
        job.setMapOutputValueClass(Put.class);
        HFileOutputFormat2.configureIncrementalLoad(job,table,
                connection.getRegionLocator(TableName.valueOf(tableName)));
        job.setOutputFormatClass(HFileOutputFormat2.class);
        HFileOutputFormat2.setOutputPath(job,new Path(args[2]));
        boolean b = job.waitForCompletion(true);
        return b?0:1;
    }
    public static void main(String[] args) throws Exception {
        Configuration configuration = HBaseConfiguration.create();
        try {
            int run = ToolRunner.run(configuration, new BulkLoadMain(), args);
        }
        catch(IOException ex){
            ex.printStackTrace();
        }
        catch(InterruptedException ex){
            ex.printStackTrace();
        }
        catch(ClassNotFoundException ex){
            ex.printStackTrace();
        }
        //System.exit(run);
    }
}