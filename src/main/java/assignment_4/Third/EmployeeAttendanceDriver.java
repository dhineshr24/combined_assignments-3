package assignment_4.Third;

import protoPackage.AttendanceProto;
import protoPackage.EmployeeProto;
import assignment_4.First.BulkLoadMain;
import assignment_4.First.LoadHFile;
import assignment_4.First.Utils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;

import java.io.IOException;
import java.util.HashMap;

public class EmployeeAttendanceDriver extends Configured implements Tool
{
    private final static String employeeTableName = "EmployeeProto";
    private final static String buildingTableName = "BuildingProto";
    private final static String employeeColumnFamily = "employeeFamily";
    private final static String buildingColumnFamily = "buildingFamily";
    private final static String attendanceTableName = "AttendanceProto";
    private final static String attendanceColumnFamily = "attendanceFamily";
    private final static String columnName = "ProtoObject";


    public static class EmployeeAttendanceMapper extends TableMapper<ImmutableBytesWritable, Result> {
        HashMap<String, String> employeeBuildingInfo = new HashMap<>();

        @Override
        protected void setup(Mapper.Context context) throws IOException {
            Connection hbaseCon = ConnectionFactory.createConnection(new HBaseConfiguration().create());
            Table table = hbaseCon.getTable(TableName.valueOf(Bytes.toBytes(employeeTableName)));
            Scan scan = new Scan();
            scan.addFamily(Bytes.toBytes(employeeColumnFamily));
            scan.setCaching(20);

            ResultScanner scanner = table.getScanner(scan);

            for (Result result = scanner.next(); (result != null); result = scanner.next()) {
                //loop for result
                for (Cell cell : result.listCells()) {
                    byte[] key = CellUtil.cloneValue(cell);
                    EmployeeProto.EmployeeDatabase dbo = EmployeeProto.EmployeeDatabase.parseFrom(key);
                    employeeBuildingInfo.put(dbo.getEmployee(0).getEmployeeId(),dbo.getEmployee(0).getBuildingCode());
                }
            }
        }

        public void map(ImmutableBytesWritable key, Result value, Mapper.Context context) throws IOException, InterruptedException {
            byte[] valueArray = value.getValue(Bytes.toBytes(attendanceColumnFamily), Bytes.toBytes(columnName));
            AttendanceProto.AttendanceDatabase dbo = AttendanceProto.AttendanceDatabase.parseFrom(valueArray);
            if(dbo.getAttendanceCount()>0)
            {
                String employeeId = dbo.getAttendance(0).getEmployeeId();
                if(!employeeId.equals("Employee_id"))
                {
                    Text outKey = new Text(employeeBuildingInfo.get(employeeId));
                    String totalAttendanceInfo = dbo.getAttendanceCount() + "," + employeeId;
                    Text outValue = new Text(totalAttendanceInfo);
//                    System.out.println("\n\n\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n\n\n");
//                    System.out.println("OutKey: " + outKey);
//                    System.out.println("OutValue: "+ outValue);
                    context.write(outKey,outValue);
                }
            }
        }
    }

    public static class EmployeeAttendanceReducer extends Reducer<Text,Text, Text, Text> {

        @Override
        public void reduce(Text key, Iterable<Text> values, Context context)
                throws IOException, InterruptedException {

            int minAttendance = 16;
            String minEmployeeId = "";
            for (Text val1: values) {
                int currentAttendance = Integer.parseInt(val1.toString().split(",")[0]);
                String currentEmployeeId = val1.toString().split(",")[1];
                if(currentAttendance<minAttendance)
                {
                    minAttendance = currentAttendance;
                    minEmployeeId = currentEmployeeId;
                }
            }
            context.write(key, new Text(": \tEmployee Id: " + minEmployeeId + "\t Count: " + minAttendance));

        }
    }

    public int run(String[] args) throws Exception {
        Scan scan = new Scan();
        scan.setCaching(500);
        scan.setCacheBlocks(false);
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf);
        job.setJarByClass(EmployeeAttendanceDriver.class);
        TableMapReduceUtil.initTableMapperJob(TableName.valueOf(attendanceTableName),scan, EmployeeAttendanceMapper.class,
                Text.class,Text.class,job);

        job.setReducerClass(EmployeeAttendanceReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        //Path path = new Path("/MapReduceJobs/AttendanceOutput");
        Path path = new Path(args[2]);
        //FileOutputFormat.setOutputPath(job, new Path(otherArgs[1]));
        FileOutputFormat.setOutputPath(job, path);

//        TableMapReduceUtil.initTableReducerJob(employeeTableName,
//                EmployeeAttendanceDriver.EmployeeAttendanceReducer.class,
//                job);
        job.waitForCompletion(true);
        return 0;
    }
    public static void main(String[] args) throws Exception {

        //Creating the AttendanceProto table in HBase to store EmpId, AttendanceInfo
        String tableName = "AttendanceProto";
        String columnFamilyName = "attendanceFamily";
        Utils hbaseObj = new Utils(tableName, columnFamilyName);
        try{
            hbaseObj.createTable();
            hbaseObj.close();
            System.out.println("\nLOG_INFO: AttendanceProto table has been created in HBase.");
        } catch (IOException e) {
            e.printStackTrace();
        }

        BulkLoadMain.main(new String[]{"AttendanceProto", args[0],args[1]});
        System.out.println("\nLOG_INFO: HFiles generated.");
        LoadHFile.main(new String[]{"AttendanceProto",args[1]});
        System.out.println("\nLOG_INFO: Loaded HFiles to AttendanceProto table.");

        //Starting MapReduce Job
        EmployeeAttendanceDriver runJob = new EmployeeAttendanceDriver();
        runJob.run(args);
    }

}
